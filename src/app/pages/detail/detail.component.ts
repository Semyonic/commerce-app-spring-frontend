import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../services/product/product.service';
import {Product} from '../../models/Product/product';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent {

  public products: Product;

  constructor(private service: ProductService, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.service.getProductById((<number>params.id)).subscribe((response: Product) => {
        this.products = response;
      });
    });
  }

}
