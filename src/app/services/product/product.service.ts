import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private readonly options;

  constructor(private http: HttpClient) {
  }

  public getAllProducts() {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(`${environment.AdminUser.name}:${environment.AdminUser.pass}`)});
    return this.http.get(`${environment.StoreBackend.Products}`, {headers});
  }

  public getProductById(id: number) {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(`${environment.AdminUser.name}:${environment.AdminUser.pass}`)});
    return this.http.get(`${environment.StoreBackend.Product}/${id}`, {headers});
  }

  public createProduct(data: object) {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(`${environment.AdminUser.name}:${environment.AdminUser.pass}`)});
    return this.http.post(`${environment.StoreBackend.Products}`, data, {headers});
  }

  public updateProduct(id: number, data: object) {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(`${environment.AdminUser.name}:${environment.AdminUser.pass}`)});
    return this.http.patch(`${environment.StoreBackend.Products}/${id}`, data, {headers});
  }

  public deleteProduct(id: number) {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(`${environment.AdminUser.name}:${environment.AdminUser.pass}`)});
    return this.http.delete(`${environment.StoreBackend.Products}/${id}`, {headers});
  }
}
