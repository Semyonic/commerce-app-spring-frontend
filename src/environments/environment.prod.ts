export const environment = {
  production: true,
  AdminUser: {
    name: 'admin',
    pass: 'azn46A!'
  },
  StoreBackend: {
    Products: 'https://pisciform-millipede-8049.dataplicity.io/api/v1/products',
    Product: 'https://pisciform-millipede-8049.dataplicity.io/api/v1/product',
    Customers: 'https://pisciform-millipede-8049.dataplicity.io/api/v1/customers'
  }
};
