import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) {
  }

  public getAllCustomers() {
    return this.http.get(`${environment.StoreBackend.Customers}`);
  }

  public getCustomerById(id: number) {
    return this.http.get(`${environment.StoreBackend.Customers}${id}`);
  }

  public createCustomer(data: object) {
    return this.http.post(`${environment.StoreBackend.Customers}`, data);
  }

  public updateCustomer(id: number, data: object) {
    return this.http.patch(`${environment.StoreBackend.Customers}${id}`, data);
  }

  public deleteCustomer(id: number) {
    return this.http.delete(`${environment.StoreBackend.Customers}${id}`);
  }
}
