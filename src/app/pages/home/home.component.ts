import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/Product/product';
import {ProductService} from '../../services/product/product.service';
import {CustomerService} from '../../services/customer/customer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  public products: Product;

  constructor(private Products: ProductService) {
    this.Products.getAllProducts().subscribe((response: Product) => {
      this.products = response;
    });
  }

}
